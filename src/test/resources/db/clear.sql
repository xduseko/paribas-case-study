delete from subscription;
delete from quotation;
delete from customer;

alter table subscription alter column id restart with 1;
alter table quotation alter column id restart with 1;
alter table customer alter column id restart with 1;
