insert into customer (first_name, last_name, middle_name, email, phone_number, birth_date)
values ('Arnold', 'Rimmer', 'Judas', 'arny@localhost', '777123456', '2150-04-18');

insert into quotation (customer_id, beginning_of_insurance, insured_amount, date_of_signing_mortgage)
values (1, '2022-02-01', '10000000', '2022-01-01');

insert into subscription (quotation_id, start_date, valid_until)
values (1, '2022-02-01', '2023-02-01');
