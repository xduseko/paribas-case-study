package com.cardif.casestudy.quotation;


import com.cardif.casestudy.ControllerTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class QuotationControllerIT extends ControllerTestBase {

    @Test
    void whenGet_thenReturnQuotation() throws Exception {
        var result = mockMvc.perform(get("/quotations/1"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        var quotation = getResponseObject(result, QuotationDTO.class);
        assertQuotation(quotation);
    }

    @Test
    void whenPost_thenCreateQuotation() throws Exception {
        var request = post("/quotations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getContent(getTestQuotation()));
        var result = mockMvc.perform(request)
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        var quotation = getResponseObject(result, QuotationDTO.class);
        assertQuotation(quotation);
    }

    private QuotationDTO getTestQuotation() {
        var quotation = new QuotationDTO();
        quotation.setCustomerId(1L);
        quotation.setBeginningOfInsurance(LocalDate.parse("2023-02-01"));
        quotation.setInsuredAmount(100_000_00L);
        quotation.setDateOfSigningMortgage(LocalDate.parse("2023-01-01"));
        return quotation;
    }

    private void assertQuotation(QuotationDTO quotation) {
        assertNotNull(quotation.getId());
        assertEquals(1L, quotation.getCustomerId());
    }
}
