package com.cardif.casestudy.subscription;


import com.cardif.casestudy.ControllerTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class SubscriptionControllerIT extends ControllerTestBase {

    @Test
    void whenGet_thenReturnSubscription() throws Exception {
        var result = mockMvc.perform(get("/subscriptions/1"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        var subscription = getResponseObject(result, SubscriptionDTO.class);
        assertSubscription(subscription);
    }

    @Test
    void whenPost_thenCreateSubscription() throws Exception {
        var request = post("/subscriptions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getContent(getTestSubscription()));
        var result = mockMvc.perform(request)
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        var subscription = getResponseObject(result, SubscriptionDTO.class);
        assertSubscription(subscription);
    }

    private SubscriptionDTO getTestSubscription() {
        var subscription = new SubscriptionDTO();
        subscription.setQuotationId(1L);
        subscription.setStartDate(LocalDate.parse("2023-02-01"));
        subscription.setValidUntil(LocalDate.parse("2024-02-01"));
        return subscription;
    }

    private void assertSubscription(SubscriptionDTO subscription) {
        assertNotNull(subscription.getId());
        assertEquals(1L, subscription.getQuotationId());
    }
}
