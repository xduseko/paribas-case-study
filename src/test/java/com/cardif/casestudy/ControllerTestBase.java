package com.cardif.casestudy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.sql.DataSource;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
public abstract class ControllerTestBase {

    @Autowired
    private DataSource dataSource;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    @BeforeEach
    void init() throws SQLException {
        executeScript("db/init.sql");
    }

    @AfterEach
    void clear() throws SQLException {
        executeScript("db/clear.sql");
    }

    protected String getContent(Object value) throws JsonProcessingException {
        return objectMapper.writeValueAsString(value);
    }

    protected <T> T getResponseObject(String content, Class<T> objectType) throws JsonProcessingException {
        return objectMapper.readValue(content, objectType);
    }

    protected <T> T getResponseObject(MvcResult result, Class<T> objectType) throws UnsupportedEncodingException,
            JsonProcessingException {
        var content = result.getResponse().getContentAsString();
        return getResponseObject(content, objectType);
    }

    private void executeScript(String path) throws SQLException {
        try (var connection = dataSource.getConnection()) {
            ScriptUtils.executeSqlScript(connection, new ClassPathResource(path));
        }
    }
}
