package com.cardif.casestudy.customer;

import com.cardif.casestudy.ControllerTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CustomerControllerIT extends ControllerTestBase {

    @Test
    void whenGet_thenReturnCustomer() throws Exception {
        var customer = fetchCustomer(1L);
        assertRimmerCustomer(customer);
    }

    @Test
    void whenPost_thenCreateCustomer() throws Exception {
        var request = post("/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getContent(getListerCustomer()));
        var result = mockMvc.perform(request)
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        var customer = getResponseObject(result, CustomerDTO.class);
        assertListerCustomer(customer);
    }

    @Test
    void whenPut_thenUpdateCustomer() throws Exception {
        var customer = updateCustomer(1L, getListerCustomer());
        assertListerCustomer(customer);
    }

    @Test
    void whenMiddleNameIsNotSet_thenItShouldBeClearedInTheDatabase() throws Exception {
        var customer = fetchCustomer(1L);
        assertNotNull(customer.getMiddleName());

        customer.setMiddleName(null);
        updateCustomer(1L, customer);

        var updatedCustomer = fetchCustomer(1L);
        assertNull(updatedCustomer.getMiddleName());
    }

    private CustomerDTO fetchCustomer(Long id) throws Exception {
        var result = mockMvc.perform(get("/customers/{id}", id))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        return getResponseObject(result, CustomerDTO.class);
    }

    private CustomerDTO updateCustomer(Long id, CustomerDTO customer) throws Exception {
        var updateRequest = put("/customers/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(getContent(customer));
        var result = mockMvc.perform(updateRequest)
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        return getResponseObject(result, CustomerDTO.class);
    }

    private CustomerDTO getListerCustomer() {
        var customerDTO = new CustomerDTO();
        customerDTO.setFirstName("David");
        customerDTO.setLastName("Lister");
        customerDTO.setEmail("dave@localhost");
        customerDTO.setPhoneNumber("777123456");
        customerDTO.setBirthDate(LocalDate.parse("2155-10-14"));
        return customerDTO;
    }

    private void assertRimmerCustomer(CustomerDTO customer) {
        assertEquals("Arnold", customer.getFirstName());
        assertEquals("Rimmer", customer.getLastName());
    }

    private void assertListerCustomer(CustomerDTO customer) {
        assertEquals("David", customer.getFirstName());
        assertEquals("Lister", customer.getLastName());
    }
}
