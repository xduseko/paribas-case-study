package com.cardif.casestudy.configuration;

import com.cardif.casestudy.customer.Customer;
import com.cardif.casestudy.customer.CustomerDTO;
import com.cardif.casestudy.quotation.Quotation;
import com.cardif.casestudy.quotation.QuotationDTO;
import com.cardif.casestudy.subscription.Subscription;
import com.cardif.casestudy.subscription.SubscriptionDTO;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        var modelMapper = new ModelMapper();

        modelMapper.createTypeMap(CustomerDTO.class, Customer.class)
                .addMappings(mapper -> mapper.skip(Customer::setId));

        modelMapper.createTypeMap(QuotationDTO.class, Quotation.class)
                .addMappings(mapper -> mapper.skip(Quotation::setId));

        modelMapper.createTypeMap(SubscriptionDTO.class, Subscription.class)
                .addMappings(mapper -> mapper.skip(Subscription::setId));

        return modelMapper;
    }
}
