package com.cardif.casestudy.quotation;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class QuotationService {

    private final QuotationRepository quotationRepository;
    private final ModelMapper modelMapper;

    public QuotationDTO getQuotation(Long id) {
        return quotationRepository.findById(id).map(this::getQuotationDTO).orElseThrow(this::getNotFoundException);
    }

    public QuotationDTO createQuotation(QuotationDTO quotationDTO) {
        var quotation = modelMapper.map(quotationDTO, Quotation.class);
        var savedQuotation = quotationRepository.save(quotation);
        return getQuotationDTO(savedQuotation);
    }

    private QuotationDTO getQuotationDTO(Quotation quotation) {
        return modelMapper.map(quotation, QuotationDTO.class);
    }

    private ResponseStatusException getNotFoundException() {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, "Quotation not found");
    }
}
