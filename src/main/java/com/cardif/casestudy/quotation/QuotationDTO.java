package com.cardif.casestudy.quotation;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class QuotationDTO {

    private Long id;

    @NotNull
    private Long customerId;

    @NotNull
    private LocalDate beginningOfInsurance;

    @NotNull
    private Long insuredAmount;

    @NotNull
    private LocalDate dateOfSigningMortgage;
}
