package com.cardif.casestudy.customer;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final ModelMapper modelMapper;

    public CustomerDTO getCustomer(Long id) {
        return customerRepository.findById(id).map(this::getCustomerDTO).orElseThrow(this::getNotFoundException);
    }

    public CustomerDTO createCustomer(CustomerDTO customerDTO) {
        var customer = modelMapper.map(customerDTO, Customer.class);
        var savedCustomer = customerRepository.save(customer);
        return getCustomerDTO(savedCustomer);
    }

    @Transactional
    public CustomerDTO updateCustomer(Long id, CustomerDTO customerDTO) {
        var customer = customerRepository.findById(id).orElseThrow(this::getNotFoundException);
        modelMapper.map(customerDTO, customer);
        return getCustomerDTO(customer);
    }

    private CustomerDTO getCustomerDTO(Customer customer) {
        return modelMapper.map(customer, CustomerDTO.class);
    }

    private ResponseStatusException getNotFoundException() {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer not found");
    }
}
