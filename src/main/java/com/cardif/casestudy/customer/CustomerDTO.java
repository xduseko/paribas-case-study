package com.cardif.casestudy.customer;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class CustomerDTO {

    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String middleName;

    @Email
    @NotNull
    private String email;

    private String phoneNumber;

    private LocalDate birthDate;
}
