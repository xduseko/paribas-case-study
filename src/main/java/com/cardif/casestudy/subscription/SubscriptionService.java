package com.cardif.casestudy.subscription;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final ModelMapper modelMapper;

    public SubscriptionDTO getSubscription(Long id) {
        return subscriptionRepository.findById(id).map(this::getSubscriptionDTO).orElseThrow(this::getNotFoundException);
    }

    public SubscriptionDTO createSubscription(SubscriptionDTO subscriptionDTO) {
        var subscription = modelMapper.map(subscriptionDTO, Subscription.class);
        var savedSubscription = subscriptionRepository.save(subscription);
        return getSubscriptionDTO(savedSubscription);
    }

    private SubscriptionDTO getSubscriptionDTO(Subscription subscription) {
        return modelMapper.map(subscription, SubscriptionDTO.class);
    }

    private ResponseStatusException getNotFoundException() {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, "Subscription not found");
    }
}
