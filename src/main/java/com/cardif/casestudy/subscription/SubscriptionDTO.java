package com.cardif.casestudy.subscription;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class SubscriptionDTO {

    private Long id;

    @NotNull
    private Long quotationId;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate validUntil;
}
