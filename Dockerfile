FROM maven:3.9.0-amazoncorretto-17 AS builder
WORKDIR /app
COPY . ./
RUN mvn -B clean verify

FROM amazoncorretto:17-alpine3.17
WORKDIR /app
COPY --from=builder /app/target/casestudy-*.jar ./
CMD java -jar casestudy-*.jar
